package com.gmail.obelouix25.player;

import java.io.File;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class PlayerLog implements Listener {
	
	
	@EventHandler
	public void PlayerJoin( PlayerJoinEvent e)
	{
	  e.setJoinMessage(ChatColor.YELLOW + e.getPlayer().getName() + " vient de se connecter");
	  e.getPlayer().sendMessage("�aAidez le serveur � vivre en faisant un dons ici : �bhttp://www.omgserv.com/fr/contribute/54198/ \n �21 tokens = 1 euros"
	  		+ "\n�4===================================================="
	  		+ "\n�6Bonjour " + "�2" + e.getPlayer().getName() +" �6!"
	  		+ "\n�4En te connectant tu accepte le reglement du serveur "
	  		+ "\n �4si tu ne l'as pas lu merci de le �llire : /rules"
	  		+ "\n�4==================================================== ");
	  File player = new File("plugins" + File.separatorChar + "ObeCommands" + File.separatorChar + "Player" + File.separatorChar + "" + e.getPlayer().getName());
	  if(!player.exists())
	  {
		player.mkdirs();
		System.out.println("le dossier de " + e.getPlayer().getName() + " a ete creer");
		Bukkit.broadcastMessage("�dBienvenue a " + "�5" + e.getPlayer().getName() +" �d sur le serveur !");
	  }
	}
	
	@EventHandler
	public void PlayerQuit( PlayerQuitEvent e)
	{
	  e.setQuitMessage(ChatColor.YELLOW + e.getPlayer().getName() + " vient de se d�connecter");
	}
}
