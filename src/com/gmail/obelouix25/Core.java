package com.gmail.obelouix25;

import java.io.File;
import java.util.logging.Logger;

import org.bukkit.craftbukkit.libs.jline.console.ConsoleReader;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.PluginLoader;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import com.gmail.obelouix25.commands.CommandKick;
import com.gmail.obelouix25.commands.commandMoney;
import com.gmail.obelouix25.commands.commandRegister;
import com.gmail.obelouix25.commands.commandTime;
import com.gmail.obelouix25.player.PlayerLog;

public class Core extends JavaPlugin {
	
	PlayerLog PlayerLog = new PlayerLog();
	CommandKick cmdKick = new CommandKick();
	commandRegister cmdRegister = new commandRegister();
	commandTime cmdTime = new commandTime();
	commandMoney cmdMoney = new commandMoney();
	
	
	@Override
	public void onEnable()
	{
		//====== Player folder =======//
		 File PlayerFile = new File(this.getDataFolder() + "/Player");
		 if(!PlayerFile.exists())
		 {
			 System.out.println("Le dossier player n'existe pas");
			 System.out.println("creation du dossier en cours...");
			 System.out.println("dossier creer avec succes !");
		 PlayerFile.mkdirs();
		 }
		 else if (PlayerFile.exists())
		 {
			 System.out.println("Le dossier player est deja existant");
		 }
		 
		System.out.printf("We recommand OMGSERV as Minecraft Server Provider");
		PluginManager pm = getServer().getPluginManager();
		pm.registerEvents(PlayerLog, this);
		pm.registerEvents(cmdKick, this);
		pm.registerEvents(cmdRegister, this);
		pm.registerEvents(cmdTime, this);
		pm.registerEvents(cmdMoney, this);
		
	}
	
	@Override
	public void onDisable()
	{
		
	}
	
	
}
